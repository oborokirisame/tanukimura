
*SaveData
	//入力されたデータを保護データの配列変数に突っ込むだけでまだファイル保存するわけではない。
	
	if entry_x1=entry_x2 | entry_z1=entry_z2 {
		//入力されたデータは面積0のおかしいデータ
		dialog "入力されたデータの\nxサイズまたはzサイズが\n0になっています。",1
		return
	}
	if fliesaveflag=0{
		//ただの新規登録
		SubstitutionForRegistration
		gosub *RGreset
	}
	if fliesaveflag=1{
		DeleteForRegistration sntk_num
		SubstitutionForRegistration
		gosub *RGreset
		
	}
	ReloadDrawMap
return
#deffunc DeleteForRegistration int hoge1
//指定したデータを消して、後ろのデータを一つずつずらす
	if motono_fliename.hoge1!"nullpo" & motono_fliename.hoge1!""{
		deletefilelists.deletecnt="data\\"+tag_name(tag.hoge1-1)+"\\"+motono_fliename.hoge1
		deletecnt+1
	}
	repeat RegistrationMax-hoge1-1,1
		xm1(cnt+hoge1-1)=xm1(cnt+hoge1)
		zm1(cnt+hoge1-1)=zm1(cnt+hoge1)
		xm2(cnt+hoge1-1)=xm2(cnt+hoge1)
		zm2(cnt+hoge1-1)=zm2(cnt+hoge1)
		tag(cnt+hoge1-1)=tag(cnt+hoge1)
		namemap(cnt+hoge1-1)=namemap(cnt+hoge1)
		username(cnt+hoge1-1)=username(cnt+hoge1)
		motono_fliename(cnt+hoge1-1)=motono_fliename(cnt+hoge1)
		rcnt=cnt
		repeat 6
		DATE(cnt,rcnt+hoge1-1)=DATE(cnt,rcnt+hoge1)
		loop
	loop
	
	
		xm1(RegistrationMax-1)=0
		zm1(RegistrationMax-1)=0
		xm2(RegistrationMax-1)=0
		zm2(RegistrationMax-1)=0
		tag(RegistrationMax-1)=0
		namemap(RegistrationMax-1)=""
		username(RegistrationMax-1)=""
		motono_fliename(RegistrationMax-1)=""
		repeat 6
		DATE(cnt,RegistrationMax-1)=0
		loop
		
	
return
#deffunc SubstitutionForRegistration 
	//空きデータがあったら突っ込むだけの簡単なお仕事
	repeat RegistrationMax
		if xm1.cnt=0 & xm2.cnt=0{
			xm1.cnt=entry_x1
			zm1.cnt=entry_z1
			xm2.cnt=entry_x2
			zm2.cnt=entry_z2
			tag.cnt=entry_comnum+1
			namemap.cnt=entry_name
			username.cnt=entry_user_name
			motono_fliename.cnt="nullpo"
			DATE(0,cnt)=gettime(0)
			DATE(1,cnt)=gettime(1)
			DATE(2,cnt)=gettime(3)
			DATE(3,cnt)=gettime(4)
			DATE(4,cnt)=gettime(5)
			DATE(5,cnt)=gettime(6)
			break
		}
	loop
return
*RGdelete

	DeleteForRegistration sntk_num
	gosub *RGreset
	ReloadDrawMap
return
*RGreset
	fliesaveflag=0
	entry_ObjRewriting "",0,0,0,0,1,"新規登録",entry_user_name

return

*savetopng
	dialog "本当にPNG出力しますか？",2
	if stat!6:return
	chdir mydir
	
	buffer 11,(1.0/b_list.b_pointa)*sizex,(1.0/b_list.b_pointa)*sizez
	pos 0,0
	gzoom (1.0/b_list.b_pointa)*sizex,(1.0/b_list.b_pointa)*sizez,10,0,0,sizex,sizez
	
	color 255,255,254
	repeat RegistrationMax
		if tag.cnt=0 : continue
		pos 1.0*((xm1.cnt-ofsetx))*(1.0/b_list.b_pointa), 1.0*((zm1.cnt-ofsetz))*(1.0/b_list.b_pointa)
		u_emes namemap.cnt
	loop
	
	pngsave "testoutput.png"
	gsel 0
return
*savetofile

	dialog "本当にセーブしますか？",2
	if stat!6:return
	chdir mydir
	
	repeat
		if deletefilelists.cnt="":break
		delete deletefilelists.cnt
		deletefilelists.cnt=""
	loop
	
	repeat RegistrationMax
		if xm1.cnt=0 & zm1.cnt=0 : break//読み取り先のデータがないのでだしゅつ
		if motono_fliename.cnt="nullpo" | motono_fliename.cnt=""{
			//新しく作られたデータなのでファイル書き出し
			create_txt_file cnt
		}
		
		
	loop
return

#deffunc create_txt_file int _index
	tmp=""
	tmp2=""
	
	tmp+=""+xm1._index+"\\,"
	tmp+=""+zm1._index+"\\,"
	tmp+=""+xm2._index+"\\,"
	tmp+=""+zm2._index+"\\,"
	tmp+=""+username._index+"\\,"
	tmp+=""+namemap._index+"\\,"
	repeat 6
		tmp+=""+DATE(cnt,_index)+"\\,"
	loop
	chdir mydir
	notesel tmp2
	noteadd tmp,0,1
	chdir mydir
	chdir "data\\"+tag_name(tag._index-1)
	notesave create_date_str(DATE(0,_index),DATE(1,_index),DATE(2,_index),DATE(3,_index),DATE(4,_index),DATE(5,_index))+namemap._index+".txt"
	
	chdir mydir
return

#defcfunc create_date_str int cds_0 , int cds_1 , int cds_3 , int cds_4 , int cds_5 ,int cds_6

return strf("%04d",cds_0)+strf("%02d",cds_1)+strf("%02d",cds_3)+strf("%02d",cds_4)+strf("%02d",cds_5)+strf("%02d",cds_6)
