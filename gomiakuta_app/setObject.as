

#deffunc FristSetObject
	entry_name=""
	entry_x1=0
	entry_z1=0
	entry_x2=0
	entry_z2=0
	entry_comnum=0
	entry_save_button_name="新規登録"
	entry_user_name=""
	
	objsize 25,25
	pos 580,480
	button gosub "+",*bairitu_plus
	pos 610,480
	button gosub "-",*bairitu_minus
	
		
	objsize 150,20
	pos 645,48 : input entry_name:on_entry_name=stat : pos 830,48 : combox entry_comnum,100,t_tag_name:on_entry_comnum=stat
	objsize 100,20
	pos 645,90 :input entry_x1 :on_entry_x1=stat:pos 750,90 :input entry_z1 : on_entry_z1=stat:pos 855,90 :input entry_user_name : on_entry_user_name=stat
	pos 645,130:input entry_x2 :on_entry_x2=stat:pos 750,130:input entry_z2 : on_entry_z2=stat
	objsize 100,30
	pos 645,155:button gosub entry_save_button_name,*SaveData: on_entry_save_button_name=stat: pos 750,155 :button gosub "選択をリセット",*RGreset: pos 855,155 :objsize 150,30:button gosub "削除(保護選択時のみ有効)",*RGdelete:on_entry_delete_button=stat
	
	objsize 150,25
	pos 645,480:button gosub "txtファイルに保存",*savetofile
	
	pos 645,510:button gosub "PNG出力",*savetopng
	
	objenable on_entry_delete_button,0
return


#deffunc entry_ObjRewriting str h_entry_name,int h_entry_x1 , int h_entry_z1,int h_entry_x2 , int h_entry_z2 ,int h_entry_comnum,str h_entry_save_button_name ,str h_entry_user_name
	objprm on_entry_name,h_entry_name
	objprm on_entry_x1,h_entry_x1
	objprm on_entry_z1,h_entry_z1
	objprm on_entry_x2,h_entry_x2
	objprm on_entry_z2,h_entry_z2
	objprm on_entry_save_button_name,h_entry_save_button_name
	objprm on_entry_user_name,h_entry_user_name
	entry_comnum=h_entry_comnum-1
	
	clrobj on_entry_comnum,on_entry_comnum
	objsel on_entry_comnum
	objsize 150,20
	pos 830,48 :combox entry_comnum,100,t_tag_name
	objsel -1
	
	objenable on_entry_delete_button,fliesaveflag
	
return

*bairitu_plus
	b_pointa+1
	if b_pointa>b_max-1 : b_pointa=b_max-1

return

*bairitu_minus
	b_pointa-1
	if b_pointa<0 : b_pointa=0

return