# 村民名簿

| 権限名 | 説明 |
|:-|:-|
| **Le**ader | 管理権限 |
| **Vi**llager | 住民 |

* 権限順、参加日順で並べています。

## Le

* Wagasana / YottaGabasana
* Harasi / ElderKraken
* Sobako_Chan
* gomiakuta / getamore
* Kirisame27

## Vi

### 定期ログインor新規入村

* Ryoken63
* killzoned0313
* Toside1515
* ST4205
* Sacchan0414
* siroharu_koyuki
* Morusun
* tagenkan
* tenngai
* chicci2525
* tama831
* yaman072
* KAZUMA_N
* CaikeDehs
* Maid_Alter
* nukopanda
* yadkari
* MentalOut
* omyon
* toranosuke7

* chucky_44 入村日不明;;


### 不定期ログインor退村リーチ
* latechama
* balxloon
* mizuki0304

### 長期オフライン許可
* Admiral_oreo
* higashi_yama

### 退村済み(退村順)

| ID | 理由 |
|:-|:-|
| StrawberryCat04 | 他村所属 |
| baraba1536 | 鯖WARN |
| hiro1112 | 鯖WARN |
| Souta0217 | 30日放置 |
| nihu | 30日放置 |
| mynstysk | 30日放置 |
| Daihye | 30日放置 |
| serenade_exe3 | 30日放置 |